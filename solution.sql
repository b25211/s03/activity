CREATE DATABASE enrollment_db;

CREATE TABLE students (
    id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(55) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE teachers (
    id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(55) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE courses (
    id INT NOT NULL AUTO_INCREMENT,
    teacher_id INT NOT NULL,
    course_name VARCHAR(55) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_courses_teacher_id 
        FOREIGN KEY (teacher_id) REFERENCES teachers (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE student_courses (
	id INT NOT NULL AUTO_INCREMENT,
    course_id INT NOT NULL,
    student_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_student_courses_course_id
		FOREIGN KEY (course_id) REFERENCES courses(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_student_courses_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Inserting data records

-- student record
INSERT INTO students(student_name)
VALUES ("Ben"),("Sullivan"),("Charlze"),("Trace"),("Builly"),("Jhin"),("Creed");

-- teachers record
INSERT INTO teachers(teacher_name)
VALUES ("Alan"),("Judy"),("Arvin");

-- student_courses record
-- INSERT INTO student_courses(course_id, student_id)
-- VALUES (1, 2),(1, 3),(1, 7),(3, 5),(3, 2),(4, 4),(4, 5);